a = 10
a.times { puts 'Hello worlds' }

james_bond = {
  :first_name   => 'James',
  :middle_name  => 'Igor',
  :last_name    => 'Bond'
}
james_bond.each_key { |key| puts james_bond[key] }