require_relative 'store_application'

StoreApplication.config do |app|

  app.name        = 'My Store'
  app.environment = :development #production

  app.admin do |admin|
    admin.email = 'admin@mystore.com'
    admin.login = 'admin'
    admin.send_info_emails_on :mondays
  end

end

p StoreApplication.environment
p StoreApplication.name
p StoreApplication.admin.email
p StoreApplication::Admin.email

@items = []
@items << AntiqueItem.new('car', price: 101, weight: 100)
@items << RealItem.new('kettle', {:weight => 10, :price => 101, :name => 'kettle'})
@items << RealItem.new('dishwasher', {:weight => 100, :price => 1001, :name => 'dishwasher'})



cart = Cart.new 'igor'
cart.add_item RealItem.new('car', {:price => 101, :weight => 100, :name => 'car'})
cart.add_item RealItem.new('car', {:price => 150, :weight => 100, :name => 'car'})
cart.add_item RealItem.new('kettle', {:price => 120, :weight => 100, :name => 'kettle'})

method = 'all_cars'
#p cart.send method
