class Order
  attr_reader :items, :placed_at, :time_spent_on_sending_email

  include ItemContainer

  def initialize
    @items = []
  end

  def place
    @placed_at = Time.now
    # thread = Thread.new do
    #   Pony.mail(
    #       :to => StoreApplication.admin.email,
    #       :from => 'My Store <***>',
    #       :via => :smtp, :via_options => {
    #         address:  'smtp.gmail.com',
    #         port:     '587',
    #         user_name: '***',
    #         password: '***',
    #         authentication: :plain,
    #         domain: 'mail.google.com'
    #       },
    #       subject: 'New order has been placed',
    #       body: 'Please check back you admin page to see it!'
    #   )
    # end
    # while thread.alive?
    #   puts '.'
    #   sleep 1
    # end
    sent_email_at = Time.now
    @time_spent_on_sending_email = sent_email_at - @placed_at
  end
end