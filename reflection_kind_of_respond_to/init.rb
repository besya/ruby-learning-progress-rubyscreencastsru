require_relative 'string'
require_relative 'item_container'
require_relative 'order'
require_relative 'item'
require_relative 'antique_item'
require_relative 'virtual_item'
require_relative 'real_item'
require_relative 'cart'

@items = []
@items << AntiqueItem.new({:price => 101, :weight => 100, :name => 'car'})
@items << RealItem.new({:weight => 10, :price => 101, :name => 'kettle'})
@items << RealItem.new({:weight => 100, :price => 1001, :name => 'dishwasher'})

cart = Cart.new 'igor'
cart.add_item RealItem.new({:price => 101, :weight => 100, :name => 'car'})
cart.add_item RealItem.new({:price => 150, :weight => 100, :name => 'car'})
cart.add_item RealItem.new({:price => 120, :weight => 100, :name => 'kettle'})

puts cart.kind_of?(Cart)              #true
puts @items[0].kind_of?(Item)         #true
puts @items[0].kind_of?(AntiqueItem)  #true
puts @items[0].class == AntiqueItem   #true
puts @items[0].class == Item          #false
puts @items[0].respond_to? :info      #true
puts @items[0].respond_to? :some      #false

puts @items[0].send :price            #108.05
puts @items[0].price                  #108.05

puts @items[0].send :tax              #22.200000000000003
#puts @items[0].tax                    #private method

method = 'all_cars'
p cart.send method
