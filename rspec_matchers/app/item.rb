class Item

  @@discount = 0.05

  def self.discount
    if Time.now.month == 11
      return @@discount + 0.1
    end
    return @@discount
  end

  attr_reader :real_price, :name

  def initialize(name, options = {})
    @real_price = options[:price]
    @name = name
  end

  def price=(value)
    @real_price = value
  end

  def info
    yield(real_price)
    yield(name)
  end

  def price
    (@real_price - @real_price*self.class.discount) + tax if @real_price
  end

  def to_s
    "#{self.name}:#{self.price}"
  end

  private
    def tax
      type_tax = self.class == VirtualItem ? 1 : 2
      cost_tax = @real_price > 5 ? @real_price*0.2 : @real_price*0.1
      cost_tax + type_tax
    end
end