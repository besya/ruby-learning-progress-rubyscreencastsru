i = 0

if i > 1
  puts '`i` is greater than 1'
end

if i < 1
  puts '`i` is less than 1'
end

i = 1

if i > 1
  puts '`i` is greater than 1'
elsif i < 1
  puts '`i` is less than 1'
else
  puts '`i` equals than 1'
end

