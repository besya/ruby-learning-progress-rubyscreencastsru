require_relative 'string'
require_relative 'item_container'
require_relative 'order'
require_relative 'item'
require_relative 'antique_item'
require_relative 'virtual_item'
require_relative 'real_item'
require_relative 'cart'

@items = []
@items << AntiqueItem.new('car', price: 101, weight: 100)
@items << RealItem.new('kettle', {:weight => 10, :price => 101, :name => 'kettle'})
@items << RealItem.new('dishwasher', {:weight => 100, :price => 1001, :name => 'dishwasher'})



cart = Cart.new 'igor'
cart.add_item RealItem.new('car', {:price => 101, :weight => 100, :name => 'car'})
cart.add_item RealItem.new('car', {:price => 150, :weight => 100, :name => 'car'})
cart.add_item RealItem.new('kettle', {:price => 120, :weight => 100, :name => 'kettle'})

method = 'all_cars'
p cart.send method
