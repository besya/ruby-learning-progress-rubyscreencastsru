require_relative 'app/store_application'

StoreApplication.config do |app|

  app.name        = 'My Store'
  app.environment = :development #production

  app.admin do |admin|
    admin.email = '***'
    admin.login = 'admin'
    admin.send_info_emails_on :mondays
  end

end

puts StoreApplication.name
unless StoreApplication.frozen?
  StoreApplication.name = 'New name'
end
unless StoreApplication.frozen?
  StoreApplication::Admin.email = 'newemail'
end
puts StoreApplication::Admin.email

@items = []
@items << AntiqueItem.new('car', price: 101, weight: 100)
@items << RealItem.new('kettle', {:weight => 10, :price => 101, :name => 'kettle'})
@items << RealItem.new('dishwasher', {:weight => 100, :price => 1001, :name => 'dishwasher'})



cart = Cart.new 'igor'
cart.add_item RealItem.new('car', {:price => 101, :weight => 100, :name => 'car'})
cart.add_item RealItem.new('car', {:price => 150, :weight => 100, :name => 'car'})
cart.add_item RealItem.new('kettle', {:price => 120, :weight => 100, :name => 'kettle'})

order = Order.new
@items.each { |i| order.add_item i }
order.place
puts order.placed_at.strftime('%b %-d, %Y %H:%M:%S') # Jan 1, 1970 15:00:00
#puts order.time_spent_on_sending_email