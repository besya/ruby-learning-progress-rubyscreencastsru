puts (2+3)*5 # 25
puts 2+3*5 # 17

puts true && true #true
puts false && true #false
puts true && false #false
puts false && false #false

puts (1 < 2) && (3 > 2)  #true
puts (1 > 2) && (3 > 2)  #false

puts true || false #true
puts false || false #false
