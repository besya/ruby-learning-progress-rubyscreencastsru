class Order
  attr_reader :items

  include ItemContainer

  def initialize
    @items = []
  end

  def place
    thread = Thread.new do
      Pony.mail(
          :to => StoreApplication.admin.email,
          :from => 'My Store <***>',
          :via => :smtp, :via_options => {
            address:  'smtp.gmail.com',
            port:     '587',
            user_name: '***',
            password: '***',
            authentication: :plain,
            domain: 'mail.google.com'
          },
          subject: 'New order has been placed',
          body: 'Please check back you admin page to see it!'
      )
    end
    while thread.alive?
      puts '.'
      sleep 1
    end
  end
end