if 2 > 1
  puts 'Hello world'
end

name = 'John'
if name == 'James' || 2 > 1
  puts 'My name is Bond'
end

unless name == 'James' && 2 > 1
  puts 'My name is Bond'
end