proc1 = Proc.new {}
p proc1

proc2 = proc {}
p proc2

x = proc { puts 'Hello world' }
x.call # Hello world

x = proc { |greeting| puts greeting }
x.call 'Hello world' # Hello world
x.call # empty

y = lambda { |greeting| puts greeting }
y.call 'Hello world' # Hello world
y.call # AgumentError: wrong number of arguments

y = lambda { return 'Hello world' }
puts y.call # Hello world

x = proc { return 'Hello world' }
puts x.call # LocalJumpError: unexpected return